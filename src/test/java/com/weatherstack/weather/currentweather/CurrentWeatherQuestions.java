package com.weatherstack.weather.currentweather;

import com.weatherstack.weather.commonactions.CommonRequestSpec;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

import static org.assertj.core.api.Assertions.assertThat;

public class CurrentWeatherQuestions {

  @Step("Verify location for the current weather response")
  public void verifyLocationInCurrentWeather(Response currentWeatherResp, String expectedLocation) {
    String currentWeatherLocation = currentWeatherResp.getBody().jsonPath().get("location.name").toString();
    assertThat(currentWeatherLocation).isEqualTo(expectedLocation);
  }

  @Step("Verify success status should be false for invalid location")
  public void verifySuccessStatusInCurrentWeatherForInvalidLocation(Response currentWeatherResp) {
    Boolean successStatus = currentWeatherResp.getBody().jsonPath().get("success");
    assertThat(successStatus).isEqualTo(false);
  }

  @Step("Verify error code and type for current weather")
  public void verifyErrorCodeAndTypeForCurrentWeather(Response currentWeatherResp, String errorCode, String errorType) {
    String errorCodeActual = currentWeatherResp.getBody().jsonPath().get("error.code").toString();
    String errorTypeActual = currentWeatherResp.getBody().jsonPath().get("error.type");
    assertThat(errorCodeActual).isEqualTo(errorCode);
    assertThat(errorTypeActual).isEqualTo(errorType);
  }
}

