package com.weatherstack.weather.currentweather;

import com.github.javafaker.Bool;
import com.weatherstack.weather.commonactions.CommonRequestSpec;
import io.restassured.response.Response;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class CurrentWeatherActions {

  @Step("Get current weather for location {0}")
  public Response getCurrentWeather(String location) {
    return SerenityRest.given().spec(CommonRequestSpec.weatherReqSpec())
        .basePath("current")
        .queryParams("query", location)
        .get().then().extract().response();
  }

  @Step("Get current weather with blank access key")
  public Response getCurrentWeatherWithBlankAccessKey(String location) {
    return SerenityRest.given().spec(CommonRequestSpec.weatherReqSpec())
            .basePath("current")
            .queryParams("query", location)
            .queryParams("access_key", "")
            .get().then().extract().response();
  }

  @Step("Get current weather with invalid access key")
  public Response getCurrentWeatherWithInvalidAccessKey(String location) {
    return SerenityRest.given().spec(CommonRequestSpec.weatherReqSpec())
            .basePath("current")
            .queryParams("query", location)
            .queryParams("access_key", "xxxxxxxxxxxxxxx")
            .get().then().extract().response();
  }
}

