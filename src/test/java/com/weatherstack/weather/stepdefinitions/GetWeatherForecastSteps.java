package com.weatherstack.weather.stepdefinitions;

import com.weatherstack.weather.commonactions.CommonQuestions;
import com.weatherstack.weather.forecast.WeatherForecastActions;
import com.weatherstack.weather.forecast.WeatherForecastQuestions;
import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

import static net.serenitybdd.rest.SerenityRest.lastResponse;


public class GetWeatherForecastSteps {

  @Steps
  WeatherForecastActions weatherForecastActions;

  @Steps
  WeatherForecastQuestions weatherForecastQuestions;

  @Steps
  CommonQuestions commonQuestions;

  @When("I call the get weather forecast endpoint for location \"(.*)\"")
  public void i_call_the_get_weather_forecast_endpoint_for_location(String location) {
    weatherForecastActions.getWeatherForecast(location);
  }

  @When("I call the get weather forecast endpoint for location \"(.*)\" with blank access key")
  public void i_call_the_get_weather_forecast_endpoint_with_blank_access_key(String location) {
    weatherForecastActions.getWeatherForecastWithBlankAccessKey(location);
  }

  @When("I call the get weather forecast endpoint for location \"(.*)\" with invalid access key")
  public void i_call_the_get_weather_forecast_endpoint_with_invalid_access_key(String location) {
    weatherForecastActions.getWeatherForecastWithInvalidAccessKey(location);
  }

  @And("the weather forecast response should have details for location \"(.*)\"")
  public void the_weather_forecast_response_should_have_details_for_provided_location(String location) {
    weatherForecastQuestions.verifyLocationInWeatherForecast(lastResponse(), location);
  }

  @And("the weather forecast response success status should be false")
  public void weather_forecast_success_status_should_be_false() {
    weatherForecastQuestions.verifySuccessStatusInWeatherForecastForInvalidLocation(lastResponse());
  }

  @And("the weather forecast response error code should be \"(.*)\" and error type should be \"(.*)\"")
  public void weather_forecast_error_code_and_type_should_be(String errorCode, String errorType) {
    weatherForecastQuestions.verifyErrorCodeAndTypeForWeatherForecast(lastResponse(), errorCode, errorType);
  }
}
