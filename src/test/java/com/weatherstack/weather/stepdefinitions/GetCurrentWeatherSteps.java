package com.weatherstack.weather.stepdefinitions;

import com.weatherstack.weather.commonactions.CommonQuestions;
import com.weatherstack.weather.currentweather.CurrentWeatherActions;
import com.weatherstack.weather.currentweather.CurrentWeatherQuestions;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

import static net.serenitybdd.rest.SerenityRest.lastResponse;


public class GetCurrentWeatherSteps {

  @Steps
  CurrentWeatherActions currentWeatherActions;

  @Steps
  CurrentWeatherQuestions currentWeatherQuestions;

  @Steps
  CommonQuestions commonQuestions;

  @When("I call the get current weather endpoint for location \"(.*)\"")
  public void i_call_the_get_current_weather_endpoint_for_location(String location) {
    currentWeatherActions.getCurrentWeather(location);
  }

  @When("I call the get current weather endpoint for location \"(.*)\" with blank access key")
  public void i_call_the_get_current_weather_endpoint_with_blank_access_key(String location) {
    currentWeatherActions.getCurrentWeatherWithBlankAccessKey(location);
  }

  @When("I call the get current weather endpoint for location \"(.*)\" with invalid access key")
  public void i_call_the_get_current_weather_endpoint_with_invalid_access_key(String location) {
    currentWeatherActions.getCurrentWeatherWithInvalidAccessKey(location);
  }

  @Then("weather details should be retrieved")
  public void weather_details_should_be_retrieved() {
    commonQuestions.responseCodeIs(200, lastResponse());
  }

  @And("the current weather response should have details for location \"(.*)\"")
  public void the_current_weather_response_should_have_details_for_provided_location(String location) {
    currentWeatherQuestions.verifyLocationInCurrentWeather(lastResponse(), location);
  }

  @And("the current weather response success status should be false")
  public void current_weather_success_status_should_be_false() {
    currentWeatherQuestions.verifySuccessStatusInCurrentWeatherForInvalidLocation(lastResponse());
  }

  @And("the current weather response error code should be \"(.*)\" and error type should be \"(.*)\"")
  public void current_weather_error_code_and_type_should_be(String errorCode, String errorType) {
    currentWeatherQuestions.verifyErrorCodeAndTypeForCurrentWeather(lastResponse(), errorCode, errorType);
  }
}
