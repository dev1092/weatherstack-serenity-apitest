package com.weatherstack.weather.stepdefinitions;

import com.weatherstack.weather.commonactions.CommonQuestions;
import com.weatherstack.weather.currentweather.CurrentWeatherActions;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

import static net.serenitybdd.rest.SerenityRest.lastResponse;


public class CommonSteps {

  @Steps
  CommonQuestions commonQuestions;


  @Then("response status should be OK")
  public void response_status_should_be_ok() {
    commonQuestions.responseCodeIs(200, lastResponse());
  }

  @Then("the schema should match with the specification defined in \"(.*)\"")
  public void the_schema_should_match_with_the_specification(String spec) {
    commonQuestions.verifyResponseSchema(lastResponse(), spec);
  }

}
