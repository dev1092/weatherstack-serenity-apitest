package com.weatherstack.weather.commonactions;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.guice.Injectors;
import net.thucydides.core.util.EnvironmentVariables;

public class CommonRequestSpec {
  /**
   * Get Request Specification for weather endpoint
   *
   * @return RequestSpecification
   */
  public static RequestSpecification weatherReqSpec() {
    EnvironmentVariables environmentVariables = Injectors.getInjector()
        .getInstance(EnvironmentVariables.class);

    String baseUrl = EnvironmentSpecificConfiguration.from(environmentVariables)
        .getProperty("baseurl");

    String apiKey = EnvironmentSpecificConfiguration.from(environmentVariables)
            .getProperty("apiKey");

    return new RequestSpecBuilder().setBaseUri(baseUrl)
        .setContentType("application/json")
            .addQueryParam("access_key", apiKey)
        .build();
  }
}
