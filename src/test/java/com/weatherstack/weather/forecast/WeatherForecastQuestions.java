package com.weatherstack.weather.forecast;

import com.weatherstack.weather.commonactions.CommonRequestSpec;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

import static org.assertj.core.api.Assertions.assertThat;

public class WeatherForecastQuestions {

  @Step("Verify location for the weather forecast response")
  public void verifyLocationInWeatherForecast(Response weatherForecastResp, String expectedLocation) {
    String weatherForecastLocation = weatherForecastResp.getBody().jsonPath().get("location.name").toString();
    assertThat(weatherForecastLocation).isEqualTo(expectedLocation);
  }

  @Step("Verify success status should be false for invalid location")
  public void verifySuccessStatusInWeatherForecastForInvalidLocation(Response weatherForecastResp) {
    Boolean successStatus = weatherForecastResp.getBody().jsonPath().get("success");
    assertThat(successStatus).isEqualTo(false);
  }

  @Step("Verify error code and type for weather forecast")
  public void verifyErrorCodeAndTypeForWeatherForecast(Response weatherForecastResp, String errorCode, String errorType) {
    String errorCodeActual = weatherForecastResp.getBody().jsonPath().get("error.code").toString();
    String errorTypeActual = weatherForecastResp.getBody().jsonPath().get("error.type");
    assertThat(errorCodeActual).isEqualTo(errorCode);
    assertThat(errorTypeActual).isEqualTo(errorType);
  }
}

