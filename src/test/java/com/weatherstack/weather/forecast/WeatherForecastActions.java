package com.weatherstack.weather.forecast;

import com.weatherstack.weather.commonactions.CommonRequestSpec;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

import java.util.HashMap;

import static org.assertj.core.api.Assertions.assertThat;

public class WeatherForecastActions {

  @Step("Get forecast for the given location")
  public Response getWeatherForecast(String location) {
    HashMap<String, Object> queryParams = new HashMap<String, Object>();
    queryParams.put("query", location);

    return SerenityRest.given().spec(CommonRequestSpec.weatherReqSpec())
        .basePath("forecast")
        .queryParams( queryParams)
        .get().then().extract().response();
  }

  @Step("Get forecast with blank access key")
  public Response getWeatherForecastWithBlankAccessKey(String location) {
    HashMap<String, Object> queryParams = new HashMap<String, Object>();
    queryParams.put("query", location);
    queryParams.put("access_key", "");

    return SerenityRest.given().spec(CommonRequestSpec.weatherReqSpec())
            .basePath("forecast")
            .queryParams( queryParams)
            .get().then().extract().response();
  }

  @Step("Get forecast with invalid access key")
  public Response getWeatherForecastWithInvalidAccessKey(String location) {
    HashMap<String, Object> queryParams = new HashMap<String, Object>();
    queryParams.put("query", location);
    queryParams.put("access_key", "xxxxxxxxxxxxxxxxx");

    return SerenityRest.given().spec(CommonRequestSpec.weatherReqSpec())
            .basePath("forecast")
            .queryParams( queryParams)
            .get().then().extract().response();
  }
}

