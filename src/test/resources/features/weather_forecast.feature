Feature: Get weather forecast
  As a weather stack api user
  I should be able to get weather forecast for given location

  Scenario: "Get weather forecast" response schema and details should match with specification for valid location
    When I call the get weather forecast endpoint for location "New York"
    Then response status should be OK
    And the schema should match with the specification defined in "weather_forecast.json"
    And the weather forecast response should have details for location "New York"

  Scenario: "Get weather forecast" response schema and details should match with specification without access key
    When I call the get weather forecast endpoint for location "New York" with blank access key
    Then the schema should match with the specification defined in "weather_forecast_error.json"
    And the weather forecast response success status should be false
    And the weather forecast response error code should be "101" and error type should be "missing_access_key"

  Scenario: "Get weather forecast" response schema and details should match with specification with invalid access key
    When I call the get weather forecast endpoint for location "New York" with invalid access key
    Then the schema should match with the specification defined in "weather_forecast_error.json"
    And the weather forecast response success status should be false
    And the weather forecast response error code should be "101" and error type should be "invalid_access_key"

  Scenario: "Get weather forecast" response schema and details should match with specification for invalid location
    When I call the get weather forecast endpoint for location "wxwxwx"
    Then the schema should match with the specification defined in "weather_forecast_error.json"
    And the weather forecast response success status should be false
    And the weather forecast response error code should be "615" and error type should be "request_failed"

  Scenario: "Get weather forecast" response schema and details should match with specification for blank location
    When I call the get weather forecast endpoint for location ""
    Then the schema should match with the specification defined in "weather_forecast_error.json"
    And the weather forecast response success status should be false
    And the weather forecast response error code should be "601" and error type should be "missing_query"