Feature: Get current weather
  As a weather stack api user
  I should be able to get current weather details for given location

  Scenario: "Get current weather" response schema and details should match with specification for valid location
    When I call the get current weather endpoint for location "New York"
    Then weather details should be retrieved
    And the schema should match with the specification defined in "current_weather.json"
    And the current weather response should have details for location "New York"

  Scenario: "Get current weather" response schema and details should match with specification without access key
    When I call the get current weather endpoint for location "New York" with blank access key
    Then the schema should match with the specification defined in "current_weather_error.json"
    And the current weather response success status should be false
    And the current weather response error code should be "101" and error type should be "missing_access_key"

  Scenario: "Get current weather" response schema and details should match with specification with invalid access key
    When I call the get current weather endpoint for location "New York" with invalid access key
    Then the schema should match with the specification defined in "current_weather_error.json"
    And the current weather response success status should be false
    And the current weather response error code should be "101" and error type should be "invalid_access_key"

  Scenario: "Get current weather" response schema and details should match with specification for invalid location
    When I call the get current weather endpoint for location "wxwxwx"
    Then the schema should match with the specification defined in "current_weather_error.json"
    And the current weather response success status should be false
    And the current weather response error code should be "615" and error type should be "request_failed"

  Scenario: "Get current weather" response schema and details should match with specification for blank location
    When I call the get current weather endpoint for location ""
    Then the schema should match with the specification defined in "current_weather_error.json"
    And the current weather response success status should be false
    And the current weather response error code should be "601" and error type should be "missing_query"