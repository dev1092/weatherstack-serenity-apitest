# Weatherstack API Automation

## Introduction

This is a test assignment project for rest api automation for endpoints available by [weatherstack](https://weatherstack.com/). The APIs provided by weatherstack can be utilized to retrieve instant, accurate weather information for any location in the world in lightweight JSON format

Tests are written using a combination of SerenityBDD, RestAssured, Cucumber, Junit & Maven.

## Prerequisites
- Java 8 or later
- Maven
- IDE (preferably IntelliJ Idea community version)

## Tech Stack
- [Serenity BDD](http://www.thucydides.info/#/) is a library helps you write better, more effective automated acceptance tests, and use these acceptance tests to produce world-class test reports and living documentation.
- Build Tool - Maven
- CI-CD - Gitlab
- RestAssured is used to make API calls and SerenityRest which is a wrapper on top of RestAssured.
- BDD Gherkin format is used to write test scripts in Cucumber feature files.
- This solution is designed in an Action-Question pattern with the code base categorized into domain model packages based on user actions and questions to understand/validate results.
- Each domain package consist of an Action class where API actions are defined and another Question class where user questions/assertions are written.
- These domain models are called from a step-definitions class which are in-turn called from BDD tests.
- A test scenario to validate API response schema has been included for each endpoint in the respective feature file. The API spec for schema comparison is placed inside "schema" folder in test resources.

## Running the tests
There are multiple ways to run the tests
- Run the command from the root of the project - 'mvn clean verify'
- Whenever there is commit in the repository, tests will be auto triggered on Gitlab (CI-CD).
- You can run from Gitlab as well whenever required.

The test results will be recorded here `target/site/serenity/index.html`.

### Add new test cases
For adding new test, understand project structure first

```Gherkin
src
  + test
    + java                          Test runners and supporting code
      + currentweather              Domain model package consisting of all actions/questions on blog posts functionality
          CurrentWeatherActions     API calls for getting current weather
          CurrentWeatherQuestions   Current Weather API calls response questions/validations
      + commonactions               Package for all common actions and questions
          CommonQuestions           All common questions/validations across all the domain models
          CommonRequestSpec         Common Request Spec for the API calls
      + stepdefinitions             Step definitions for the BDD feature
    + resources
      + features                    Feature files directory
          current_weather.feature   Feature containing BDD scenarios
      + schema                      Folder containing json schema for API schema validation
      Serenity.conf                 Configurations file

```
Adding tests for new API say xyz
- Add a feature file under src/test/resources/features say xyz.feature for xyz api.
- Add response schemas under src/test/resources/schema for the xyz api.
- Add a package xyz under src/test/java/com/weatherstack/weather and add files inside the same package i.e. XYZActions.java and XYZQuestions.java
- Add the required methods inside the classes created in above step.

### Additional configurations

Additional command line parameters can be passed for switching the application environment.
```
$ mvn clean verify -Denvironment=dev
```
Configurations to for different environments are set in the `test/resources/serenity.conf` file. In real time projects each environment can be configured with its baseurl to run the tests based on different environments.
```
environments {
  default {
    baseurl = "http://api.weatherstack.com",
    apiKey = "xxxxxxxx"
  }
  test {
    baseurl = "http://api.weatherstack.com",
    apiKey = "xxxxxxx"
  }
  staging {
    baseurl = "http://api.weatherstack.com",
    apiKey = "xxxxxxxx"
  }
}
```

### Endpoints Interaction
You can find full documentation [here](https://weatherstack.com/documentation)
- Current Weather
  http://api.weatherstack.com/current
  ? access_key = YOUR_ACCESS_KEY
  & query = New York

To query the weatherstack API for real-time weather data in a location of your choice, simply attach your preferred location to the API's current endpoint as seen in the example request below. Depending on your subscription, you can also make a bulk location request by passing multiple semicolon-separated locations to the API URL.

- Forecast Weather
  http://api.weatherstack.com/forecast
  ? access_key = YOUR_ACCESS_KEY
  & query = New York
  & forecast_days = 1
  & hourly = 1

The weatherstack is capable of returning weather forecast data for up to 14 days into the future. To get weather forecasts, simply use the API's forecast and define your preferred number of forecast days using the forecast_days parameter.

### Points to be noted
The weatherstack account used for this automation is having free subscription for apis calls.
The access key used has below limitations:
- Can be utilized for maximum 250 calls.
- Can access only two apis i.e. current weather and forecast weather.
- For forecast weather can not use the feature like forecast days and hourly.